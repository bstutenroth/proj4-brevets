"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km <= 200:
        ot = control_dist_km // 34
    elif control_dist_km <= 400 and control_dist_km > 200:
        ot = control_dist_km // 32
    elif control_dist_km <= 600 and control_dist_km > 400:
        ot = control_dist_km // 30
    elif control_dist_km <= 1000 and control_dist_km > 600:
        ot = control_dist_km // 28
    elif control_dist_km <= 1300 and control_dist_km > 1000:
        ot = control_dist_km // 26
    return ot


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km <= 200:
        ct = control_dist_km // 15
    elif control_dist_km <= 400 and control_dist_km > 200:
        ct = control_dist_km // 15
    elif control_dist_km <= 600 and control_dist_km > 400:
        ct = control_dist_km // 15
    elif control_dist_km <= 1000 and control_dist_km > 600:
        ct = control_dist_km // 11.428
    elif control_dist_km <= 1300 and control_dist_km > 1000:
        ct = control_dist_km // 13.333
    return ct

