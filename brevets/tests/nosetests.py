"""
nose tests for acp_times.py

"""
import acp_times
import nose
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

def test_200():
    brevet_start_time = 2019-11-12T00:00:00+00:00
    assert open_time(200,50,brevet_start_time)==2019-11-12T01:28:00+00:00
    assert close_time(200,50,brevet_start_time)==2019-11-12T03:30:00+00:00
    assert open_time(200,150,brevet_start_time)==2019-11-12T04:25:00+00:00
    assert close_time(200,150,brevet_start_time)==2019-11-12T10:00:00+00:00
    assert open_time(200,200,brevet_start_time)==2019-11-12T05:53:00+00:00
    assert close_time(200,200,brevet_start_time)==2019-11-12T13:30:00+00:00

def test_300():
    brevet_start_time = 2019-11-12T00:00:00+00:00
    assert open_time(300,0,brevet_start_time)==2019-11-12T00:00:00+00:00
    assert close_time(300,0,brevet_start_time)==2019-11-12T01:00:00+00:00
    assert open_time(300,150,brevet_start_time)==2019-11-12T04:25:00+00:00
    assert close_time(300,150,brevet_start_time)==2019-11-12T10:00:00+00:00
    assert open_time(300,300,brevet_start_time)==2019-11-12T09:00:00+00:00
    assert close_time(300,300,brevet_start_time)==2019-11-12T20:00:00+00:00

def test_400():
    brevet_start_time = 2019-11-12T00:00:00+00:00
    assert open_time(200,50,brevet_start_time)==2019-11-12T00:00:00+00:00
    assert close_time(200,50,brevet_start_time)==2019-11-12T01:00:00+00:00
    assert open_time(200,175,brevet_start_time)==2019-11-12T05:09:00+00:00
    assert close_time(200,175,brevet_start_time)==2019-11-12T11:40:00+00:00
    assert open_time(200,400,brevet_start_time)==2019-11-12T12:08:00+00:00
    assert close_time(200,400,brevet_start_time)==2019-11-12T03:00:00+00:00

def test_600():
    brevet_start_time = 2019-11-12T00:00:00+00:00
    assert open_time(200,50,brevet_start_time)==2019-11-12T00:00:00+00:00
    assert close_time(200,50,brevet_start_time)==2019-11-12T01:00:00+00:00
    assert open_time(200,480,brevet_start_time)==2019-11-12T14:48:00+00:00
    assert close_time(200,480,brevet_start_time)==2019-11-12T08:00:00+00:00
    assert open_time(200,600,brevet_start_time)==2019-11-12T18:48:00+00:00
    assert close_time(200,600,brevet_start_time)==2019-11-12T16:00:00+00:00


def test_1000():
    brevet_start_time = 2019-11-12T00:00:00+00:00
    assert open_time(1000,500,brevet_start_time)==2019-11-12T15:28:00+00:00
    assert close_time(1000,500,brevet_start_time)==2019-11-12T09:20:00+00:00
    assert open_time(1000,800,brevet_start_time)==2019-11-12T01:57:00+00:00
    assert close_time(1000,800,brevet_start_time)==2019-11-12T09:30:00+00:00
    assert open_time(1000,1000,brevet_start_time)==2019-11-12T09:05:00+00:00
    assert close_time(1000,1000,brevet_start_time)==2019-11-12T03:00:00+00:00

